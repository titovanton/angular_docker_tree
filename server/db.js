var mongoose = require('mongoose');
mongoose.connect('mongodb://mongo/secret_db');
var db;
var exp = {
	init:function(done){
		db = mongoose.connection;
		db.on('error', function(err){
			console.error('DB err',err);
		});
		db.once('open', function (err) {
			console.log('DB opened');
			done(null)
		});
	}
};

module.exports = exp;