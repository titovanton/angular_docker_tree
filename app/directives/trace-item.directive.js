angular.module('demoapp').directive('traceItem', ['$interval',function($interval) {
	return {
		restrict: 'E',
		scope: {data:'='},
		templateUrl: 'directives/trace-item.tmpl.html',
		link: function (scope, element) {
			//
			scope.formatedData = getFormatedDate();
			$interval(function(){
				scope.formatedData = getFormatedDate();
			},1000);

			function getFormatedDate(date){
				return Math.round((new Date().getTime() - scope.data.time)/1000)+' sec';
			}
		}
	};
}]);
