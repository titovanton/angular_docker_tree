
angular.module('demoapp',[
		'ngSanitize',
		'ui.bootstrap',
		'ui.router',
		'ui.tree'
])
	.config(function($stateProvider, $urlRouterProvider,treeConfig) {

		//treeConfig.defaultCollapsed = true; // collapse nodes by default

		$stateProvider
			.state('main', {
				url: '/',
				controller: 'MainCtrl',
				templateUrl: 'pages/main.tmpl.html'
			})
			.state('tracer', {
				url: '/tracer',
				controller: 'TracerCtrl',
				templateUrl: 'pages/tracer.tmpl.html'
			})
			.state('useless', {
				url: '/useless',
				controller: 'UselessCtrl',
				templateUrl: 'page/useless.tmpl.html'
			});


		// if none of the above states are matched, use this as the fallback
		  $urlRouterProvider.otherwise('/');
	});

console.log('app');