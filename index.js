var express 			= require('express');
var app 				= express();

var db 					= require('./server/db.js');

app.use('/', express.static(__dirname + '/app'));
db.init(function(err){
	if(err){

	}else{
		startServer()
	}
});

function startServer(){
	var server = app.listen(process.env.PORT || 3000, function () {
		var host = server.address().address;
		var port = server.address().port;

		console.log('Example app listening at http://%s:%s', host, port);
	});
}
